package co.istad.ams;

import org.springframework.stereotype.Component;

@Component
public class ArticleUtil {
    public String getArticle(Article article) {
        return "getArticle " + article.getTitle();
    }
}
