package co.istad.ams;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArticleBeanConfig {

    @Bean("article1")
    Article beanArticle1() {
        Article article = new Article();
        article.setId(1);
        article.setTitle("Article 001");
        return article;
    }

    @Bean("article2")
    Article beanArticle2() {
        Article article = new Article();
        article.setId(2);
        article.setTitle("Article 002");
        return article;
    }

}
