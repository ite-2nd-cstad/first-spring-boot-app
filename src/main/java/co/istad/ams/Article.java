package co.istad.ams;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class Article {

    private Integer id;
    private String title;

}
