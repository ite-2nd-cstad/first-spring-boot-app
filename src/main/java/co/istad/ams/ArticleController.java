package co.istad.ams;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ArticleController {

    // Declare dependency
    private final Article article;
    // Inject
    public ArticleController(@Qualifier("article1") Article article) {
        this.article = article;
    }

    private Article myArticle;

    @Autowired
    public void setMyArticle(@Qualifier("article2") Article myArticle) {
        this.myArticle = myArticle;
    }

    @GetMapping("/article")
    String viewArticle(Model model) {
        System.out.println(myArticle);
        model.addAttribute("article", article);
        return "article";
    }

}
