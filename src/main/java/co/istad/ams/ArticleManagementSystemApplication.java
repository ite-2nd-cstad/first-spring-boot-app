package co.istad.ams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticleManagementSystemApplication.class, args);

		Article article = new Article();
		article.setId(1);
		article.setTitle("Title");

		System.out.println(article.getId());
		System.out.println(article.getTitle());
	}

}
