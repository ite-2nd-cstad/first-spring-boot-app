package co.istad.ams;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ArticleManagementSystemApplicationTests {

	@Autowired
	@Qualifier("article1")
	private Article article;

	@Test
	void testLombok() {
		Assertions.assertEquals(1, article.getId());
	}

	@Test
	void testLombok2() {
		Article article = new Article();
		article.setId(1);
		article.setTitle("Title");

		System.out.println(article.getId());
		System.out.println(article.getTitle());
		Assertions.assertEquals(1, article.getId());
	}

}
